import java.util.Scanner;

public class LoopsTest {
	public static void main(String args[]) {

		// for(int i = 0; i < 10; i++)
		// {
		// System.out.println(i);
		// }
		//
		// int i = 0;
		//
		// while(i<10)
		// {
		//
		// System.out.println(i);
		// i++;
		//
		// }
		//
		//
		// int i = 0;
		//
		// //a do while loop will carry out an operation first and then check if
		// it is FALSE/TRUE afterwards.
		//
		// do {
		//
		//
		// System.out.println(i);
		// i++;
		//
		// }
		//
		// while(i<10);

//		int i = 2;
//		while (i >= -4) {
//			if (i>0) {
//				for (int h = i; h > 0; h--) {
//					System.out.println(i * h);
//				}
//			}
//			int j = i; // value of i is dependent on the value of j. What i is,
//						// j will be affected by.
//			do {
//				System.out.println(j);
//			} while (j > 2);
//			i--;
//		}

		/*
		 * 
		 * for(int h = i; i > 0; i++) --> example of an infinite loop
		 * 
		 * 
		 * 4 2 2 1 1 0 -1 -2 -3 -4
		 */
		
//		int number = 1;
//		if(number > 10 && number < 50 || number == 1)
//			System.out.println(number);
		
//		int sum = 0;
//		for(int i = 0; i < 100; i++)
//		{
//			sum += i;
//			if(i == 50)
//				break;
//		}
//		System.out.println(sum);
//		for(int i = 0; i < 5; i++)
//		{
//			System.out.println(i);
//			for(int j = i; j < 3; j++)
//			{
//				System.out.println(j);
//				for(int k = i; k < j; k++)
//				{
//					System.out.println(k);
//				}
//			}
//		}
		
//		Random r = new Random();
//		for(int i = 0; i < 10; i++)
//		{
//			System.out.println(Math.round(r.nextDouble()*100000));
//		}
		
//		for(int i = 1; i<=5; i++)
//		{
//			for (int j = i; j>0; j--)
//			{
//				System.out.print("*");
//				
//			}
//			
//			System.out.println();
//		}
		
//		Scanner kbReader = new Scanner(System.in);
//		//String text = kbReader.next();
//		String userInput ="";
//		
//		do
//		{
//			
//			System.out.println("Enter some text: ");
//			userInput = kbReader.nextLine();
//			
//			
////			
////			if (kbReader.next().equals("Quit"))
////			{
////				break;
////			}
//			
//		}
//		
//		while(!(userInput.equalsIgnoreCase("Quit")));
		
//		Scanner kbReader = new Scanner(System.in);
//		//String text = kbReader.next();
//		String userInput ="";
//		
//		do
//		{
//			
//			System.out.println("Enter some text: ");
//			userInput = kbReader.nextLine();
//			
//			
//			
//			if ((userInput.equalsIgnoreCase("Quit")))
//			{
//				break;
//			}
//			
//		}
//		
//		while(true);
//		for(int i = 0; i <= 5; i+=2)
//		{
//			System.out.println(i);
//		}
	}
	
	

}
